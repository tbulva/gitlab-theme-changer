import { getSettings, updateUserTheme } from "./utils";

(async function () {
  const { gitlabUrl } = await getSettings();

  if (gitlabUrl !== window.location.origin) {
    return;
  }

  window
    .matchMedia("(prefers-color-scheme: dark)")
    .addEventListener("change", async (e) => {
      const theme = e.matches ? "dark" : "light";
      await updateUserTheme(theme);
      window.location.reload();
    });
})();
