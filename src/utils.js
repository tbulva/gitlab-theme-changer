import {
  STORAGE_API_KEY,
  STORAGE_GITLAB_URL,
  STORAGE_USERS,
  STORAGE_SELECTED_USER,
  STORAGE_USER_THEME_PRESET,
} from "./constants";

export const readStorage = async (key) => {
  const { [key]: result } = await chrome.storage.sync.get(key);
  let parsedResult;

  try {
    parsedResult = JSON.parse(result);
  } catch (error) {
    console.warn("JSON parsing error", error);
    parsedResult = result;
  }
  return parsedResult;
};

export const writeStorage = (key, value) => {
  if (typeof value !== "string") {
    value = JSON.stringify(value);
  }
  return chrome.storage.sync.set({ [key]: value });
};

export const IsApiTokenDirty = (currentValue) => {
  return readStorage(STORAGE_API_KEY) === currentValue;
};

export const getUsers = async () => {
  const apiToken = await readStorage(STORAGE_API_KEY);
  const gitlabUrl = await readStorage(STORAGE_GITLAB_URL);
  let done = false;

  const fetchUrl = new URL("/api/v4/users", gitlabUrl);
  fetchUrl.searchParams.set("access_token", apiToken);
  fetchUrl.searchParams.set("per_page", 100);
  try {
    const result = await fetch(fetchUrl.toString(), {
      method: "GET",
      headers: {
        "PRIVATE-TOKEN": apiToken,
      },
    });

    if (result.status === 200) {
      const users = await result.json();
      const shortUsers = users.map((user) => ({
        id: user.id,
        username: user.username,
        theme_id: user.theme_id,
        color_scheme_id: user.color_scheme_id,
      }));

      writeStorage(STORAGE_USERS, shortUsers);
      done = true;
    }
  } catch (error) {
    console.warn("getUsers fetch error:", error);
  }

  return done;
};

export const getCurrentThemeAndSyntax = async () => {
  const apiToken = await readStorage(STORAGE_API_KEY);
  const gitlabUrl = await readStorage(STORAGE_GITLAB_URL);
  const selectedUser = await readStorage(STORAGE_SELECTED_USER);
  let userThemeAndSyntaxSettings = {};

  const fetchUrl = new URL(`/api/v4/users/${selectedUser}`, gitlabUrl);
  fetchUrl.searchParams.set("access_token", apiToken);

  try {
    const result = await fetch(fetchUrl.toString(), {
      method: "GET",
      headers: {
        "PRIVATE-TOKEN": apiToken,
      },
    });

    if (result.status === 200) {
      const { theme_id, color_scheme_id } = await result.json();
      userThemeAndSyntaxSettings = {
        theme_id,
        color_scheme_id,
      };
    }
  } catch (error) {
    console.warn("getUsers fetch error:", error);
  }

  return userThemeAndSyntaxSettings;
};

export const getSettings = async () => {
  const userThemePreset = await readStorage(STORAGE_USER_THEME_PRESET);
  const selectedUserId = await readStorage(STORAGE_SELECTED_USER);
  const apiToken = await readStorage(STORAGE_API_KEY);
  const gitlabUrl = await readStorage(STORAGE_GITLAB_URL);
  const usersList = await readStorage(STORAGE_USERS);

  return {
    userThemePreset,
    selectedUserId,
    apiToken,
    gitlabUrl,
    usersList,
  };
};

export const updateUserTheme = async (systemPreferedTheme) => {
  const settings = await getSettings();

  const fetchUrl = new URL(
    `/api/v4/users/${settings.selectedUserId}`,
    settings.gitlabUrl,
  );

  try {
    const result = await fetch(fetchUrl.toString(), {
      method: "PUT",
      headers: {
        "PRIVATE-TOKEN": settings.apiToken,
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        theme_id:
          settings?.userThemePreset?.[settings?.selectedUserId]?.[
            systemPreferedTheme
          ]?.theme,
        color_scheme_id:
          settings?.userThemePreset?.[settings?.selectedUserId]?.[
            systemPreferedTheme
          ]?.syntax,
      }),
    });
    const resultJson = await result.json();

    if (result.status !== 200) {
      console.warn("updateUserTheme failed", resultJson);
    }
  } catch (error) {
    console.warn("getUsers fetch error:", error);
  }

  return true;
};

export const validateApiKey = (apiKey) => {
  let regex = /^[a-zA-Z0-9\-_]+$/;

  return regex.test(apiKey);
};

export const validateApiUrl = (apiUrl) => {
  let regex = /^(https?:\/\/)?([a-zA-Z0-9.-]+)(:[0-9]{1,5})?\/?$/;

  return regex.test(apiUrl);
};

export const slugify = (text) =>
  text
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "-")
    .replace(/[^\w-]+/g, "")
    .replace(/--+/g, "-")
    .replace(/^-+/, "")
    .replace(/-+$/, "");
