export const STORAGE_API_KEY = "STORAGE_API_KEY";
export const STORAGE_GITLAB_URL = "STORAGE_GITLAB_URL";
export const STORAGE_USERS = "STORAGE_USERS";
export const STORAGE_SELECTED_USER = "STORAGE_SELECTED_USER";
export const STORAGE_THEME_DARK = "STORAGE_THEME_DARK";
export const STORAGE_SYNTAX_DARK = "STORAGE_SYNTAX_DARK";
export const STORAGE_THEME_LIGHT = "STORAGE_THEME_LIGHT";
export const STORAGE_SYNTAX_LIGHT = "STORAGE_SYNTAX_LIGHT";
export const STORAGE_USER_THEME_PRESET = "STORAGE_USER_THEME_PRESET";

export const THEME_SELECTOR_DATA = [
  {
    id: "1",
    name: "Indigo",
  },
  {
    id: "6",
    name: "Light Indigo",
  },
  {
    id: "4",
    name: "Blue",
  },
  {
    id: "7",
    name: "Light Blue",
  },
  {
    id: "5",
    name: "Green",
  },
  {
    id: "8",
    name: "Light Green",
  },
  {
    id: "9",
    name: "Red",
  },
  {
    id: "10",
    name: "Light Red",
  },
  {
    id: "2",
    name: "Gray",
  },
  {
    id: "3",
    name: "Light Gray",
  },
  {
    id: "11",
    name: "Dark Mode (alpha)",
  },
];

export const SYNTAX_SELECTOR_DATA = [
  {
    id: "1",
    name: "Light",
  },
  {
    id: "2",
    name: "Dark",
  },
  {
    id: "3",
    name: "Solarized Light",
  },
  {
    id: "4",
    name: "Solarized Dark",
  },
  {
    id: "5",
    name: "Monokai",
  },
  {
    id: "6",
    name: "None",
  },
];
