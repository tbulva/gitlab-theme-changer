# Gitlab Theme Changer

This Chrome Browser extension will auto toggles dark and light gitlab theme based on the system settings.

![demo](./demo-small.gif)

## Installation

1. clone this repo
2. cd in the directory
3. `npm install`
4. `npm run build`
5. this will produce `dist` directory
6. - OR - only download extension.zip from this repo and unzip
7. You need to enable developer mode in the Chrome and then click "Load unpacked" button. Select `dist` directory with this extension.

![install](./install.gif)

## Setup

Extension will guide you through setup steps. 

1. add url of you instance
2. add api key
3. select user
4. select set of light and dark themes and syntax

![setup](./setup.gif)

## How it works

Extension is using gitlab public API to access and update user settings. Therefor you habe to create a API access token.

## Limitations

The most severe limitation is that you have to have admin access to your gitlab instance.

